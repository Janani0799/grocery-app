/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import SignUp from './src/screens/authentication/SignUp';
import Login from './src/screens/authentication/Login';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
