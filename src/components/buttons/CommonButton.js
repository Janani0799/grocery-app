import React from 'react';
import {View, TouchableOpacity, Text} from 'react-native';
import { ScaledSheet } from "react-native-size-matters";
import {adjust, COLORS, FONTS, SIZES} from '../../constants';
const CommonButton = ({text, onPress, buttonColor,width,secondary}) => {
  return (
    <TouchableOpacity
      onPress={() => onPress()}
      style={[styles.primaryButton,{backgroundColor:buttonColor,width:width}]}>
      <Text
        style={secondary ? styles.secondaryText : styles.primaryText}>
        {text}
      </Text>
    </TouchableOpacity>
  );
};
const styles = ScaledSheet.create({
  primaryButton: {
    justifyContent: 'center',
    alignItems: 'center',
    height: "45@vs",
    borderRadius: SIZES.radius / 3,
    paddingHorizontal: SIZES.padding2,
    //width: SIZES.width,
  },
//   secondaryButton: {
//     justifyContent: 'center',
//     alignItems: 'center',

//     // height: 40,
//     borderRadius: SIZES.radius / 6,
//     paddingHorizontal: SIZES.padding2,
//   },
  primaryText: {
    ...FONTS.h5,
    fontSize:adjust(14),
    color: COLORS.whiteColor,
    paddingVertical: SIZES.base,
  },
  secondaryText: {
    ...FONTS.h5,
    fontSize:adjust(14),
    color: COLORS.primaryColor,
    paddingVertical: SIZES.base,
  },
//   secondaryText: {
//     ...FONTS.h5,
//     fontSize:SIZES.body6,
//     letterSpacing:0.5,
//     color: COLORS.primaryColor,
//     paddingVertical: SIZES.base/1.2,
//   },
});
export default CommonButton;
