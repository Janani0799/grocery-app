import React from "react";
import { Text, TouchableOpacity, StyleSheet } from "react-native";
import SpinnerButton from "react-native-spinner-button";
import { COLORS, SIZES, FONTS } from "../../constants";
// Button
const CommonSpinnerButton = ({
  text,
  onPress,
  isLoading,
  type,
  disable = false,
  buttonStyle,
  textStyle,
  disabled,
}) => {
  return (
    <SpinnerButton
      buttonStyle={buttonStyle}
      isLoading={isLoading}
      onPress={onPress}
      spinnerType={"MaterialIndicator"}
      disabled={disabled}
    >
      <Text style={textStyle}>{text}</Text>
    </SpinnerButton>
  );
};
export default CommonSpinnerButton;