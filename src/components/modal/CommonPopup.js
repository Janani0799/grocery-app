import {StyleSheet, Text, View, Alert, Dimensions} from 'react-native';
import React, {useState} from 'react';
import Modal from 'react-native-modal';
import {ScaledSheet} from 'react-native-size-matters';
import {TextInputField} from '../../components/TextInputField';
import {TextInput} from 'react-native-paper';
import {
  deviceHeight,
  deviceWidth,
  FONTS,
  palette,
  SIZES,
  COLORS,
  adjust,
} from '../../constants/Theme';
import CommonSpinnerButton from '../buttons/CommonSpinnerButton';

var {height, width} = Dimensions.get('window');

export const CommonPopup = ({
  isVisible,
  title,
  subtitle,
  text,
  onPressNo,
  onPressYes,
  successButtonText,
  negativeButtonText,
  type,
}) => {
  //const [type, setType] = useState(0);
  return (
    <Modal
      animationIn={'slideInUp'}
      isVisible={isVisible}
      style={styles.container}>
      <View style={styles.containerStyle}>
        <Text style={styles.title}>{title}</Text>
        <View style={styles.row}>
          <Text style={styles.subtitle}>{subtitle}</Text>
          {type === 1 ? <Text style={styles.text}>Rs: {text}</Text> : null}
        </View>

        <View style={styles.buttonContainer}>
          <CommonSpinnerButton
            buttonStyle={styles.ButtonStyle_No}
            isLoading={false}
            text={negativeButtonText}
            textStyle={styles.buttonTextStyle_No}
            onPress={onPressNo}
          />
          <CommonSpinnerButton
            buttonStyle={styles.ButtonStyle_yes}
            isLoading={false}
            text={successButtonText}
            textStyle={styles.buttonTextStyle_yes}
            onPress={onPressYes}
          />
        </View>
      </View>
    </Modal>
  );
};
const styles = ScaledSheet.create({
  containerStyle: {
    width: width - 30,

    backgroundColor: 'white',
    borderRadius: '4@msr',
    justifyContent: 'center',
    alignSelf: 'center',
    paddingVertical: '20@vs',
  },

  container: {
    height: '600@vs',
  },
  title: {
    ...FONTS.h4,
    paddingHorizontal: '10@msr',
    paddingVertical: '5@vs',
    color: COLORS.blackColor,
  },
  subtitle: {
    ...FONTS.h5,
    paddingHorizontal: '10@msr',
    paddingVertical: '5@vs',
    color: COLORS.blackColor,
  },
  text: {
    ...FONTS.h5,
    fontWeight: '700',
    textAlign: 'center',
    // paddingHorizontal: "10@msr",
    paddingVertical: '5@vs',
    color: COLORS.primaryColor,
  },
  row: {flexDirection: 'row', paddingVertical: '10@vs'},
  buttonContainer: {
    flexDirection: 'row',
    marginVertical: '5@vs',
    justifyContent: 'space-between',
  },
  //buttons
  //no
  buttonTextStyle_No: {
    ...FONTS.P0,
    fontSize: adjust(12),
    color: COLORS.whiteColor,
  },
  ButtonStyle_No: {
    height: '40@vs',
    width: '135@s',
    borderRadius: adjust(5),
    backgroundColor: COLORS.secondaryColor,
  },
  //no
  //yes
  buttonTextStyle_yes: {
    ...FONTS.P0,
    fontSize: adjust(12),
    color: COLORS.whiteColor,
  },
  ButtonStyle_yes: {
    height: '40@vs',
    width: '135@s',
    borderRadius: adjust(5),
    backgroundColor: COLORS.primaryColor,
  },
});
