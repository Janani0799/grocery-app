import React from 'react';
import {View, Text, TouchableOpacity, TextInput} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {COLORS} from '../../constants';

export const TextInputField = ({
  placeHolder,
  onChangeText,
  value = '',
  secureTextEntry,
  keyboardType,
  autoCapitalize,
  height,
  width,
  paddingRight,
  returnKeyType,
  onSubmitEditing,
  blurOnSubmit,
  sref,
}) => {
  return (
    <View style={styles.containercontainer}>
      <TextInput
        value={value}
        onChangeText={onChangeText}
        placeholder={placeHolder}
        style={[
          styles.textInput,
          {height: height, width: width, paddingRight: paddingRight},
        ]}
        placeholderTextColor={COLORS.textColor}
        secureTextEntry={secureTextEntry}
        keyboardType={keyboardType}
        autoCapitalize={autoCapitalize}
        returnKeyType={returnKeyType}
        onSubmitEditing={onSubmitEditing}
        blurOnSubmit={blurOnSubmit}
        ref={sref}
      />
      <View style={{height: 18}} />
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },
  textInput: {
    borderBottomColor: COLORS.lightGrey,
    borderBottomWidth: '1@s',
    color: COLORS.blackColor,
  },
});
