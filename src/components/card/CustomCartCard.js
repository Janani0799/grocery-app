import {
  Text,
  SafeAreaView,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import React from 'react';
import {FONTS, COLORS, adjust, Vector, Images, SIZES} from '../../constants';
import {ScaledSheet} from 'react-native-size-matters';

export const CustomCartCard = ({
  title,
  image,
  price,
  pack,
  onPressMinus,
  onPressPlus,
  totalNum,
  onClick,
}) => {
  return (
    <SafeAreaView style={styles.container}>
      <View style={{flexDirection: 'row'}}>
        <View style={{flex: 0.7}}>
          <Text style={styles.title}>{'Natural Red Apple'}</Text>
        </View>
        <TouchableOpacity style={{flex: 0.7, alignItems: 'flex-end'}}>
          {Vector.close}
        </TouchableOpacity>
      </View>
      <Text style={styles.quantity}>{'1kg'}</Text>
      <View style={styles.priceView}>
        <View style={{flex: 0.7}}>
          <View style={styles.ButtonStyle}>
            <View style={styles.button}>
              <View style={{justifyContent: 'center'}}>
                <TouchableOpacity onPress={() => decrease()}>
                  <Icons name="minus" size={20} style={styles.iconMinus} />
                </TouchableOpacity>
              </View>
              <View style={{justifyContent: 'center'}}>
                <Text style={styles.total}>{total}</Text>
              </View>
              <View style={{justifyContent: 'center'}}>
                <TouchableOpacity onPress={() => increase()}>
                  <Icons name="plus" style={styles.iconPlus} size={20} />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>

        <View
          style={{
            flex: 0.35,
            alignItems: 'flex-end',
            marginTop: SIZES.base - 5,
          }}>
          <Text style={styles.title}>{'Rs. 100'}</Text>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },

  cardView: {
    marginTop: '13@vs',
    borderRadius: '15@msr',
    width: '145@s',
    height: '155@vs',
    borderWidth: '0.8@s',
    bottom: 5,
    marginLeft: '12@s',
    paddingHorizontal: '20@s',
    alignItems: 'center',
  },

  image: {
    width: '83@s',
    height: '80@vs',
    marginHorizontal: '3@s',
    marginVertical: '3@vs',
    borderTopLeftRadius: '4@msr',
    borderBottomLeftRadius: '4@msr',
  },
  titletxt: {
    fontSize: adjust(14),
    fontWeight: '500',
    color: COLORS.blackColor,
    textAlign: 'center',
  },
  txt: {
    ...FONTS.body6,
    fontSize: adjust(10),

    color: COLORS.blackColor,
  },

  buttonTextStyle: {
    ...FONTS.P0,
    color: COLORS.whiteColor,
  },

  title: {
    //...FONTS.P0,
    fontSize: adjust(16),
    fontWeight: '500',
    color: COLORS.blackColor,
  },
  quantity: {
    ...FONTS.body4,
    fontSize: adjust(13),
    lineHeight: '20@vs',
    color: COLORS.textColor,
  },
  ButtonStyle: {
    height: '32@vs',
    width: '87@s',
    borderRadius: '5@msr',
    backgroundColor: COLORS.whiteColor,
    justifyContent: 'center',
    alignSelf: 'flex-start',
  },

  button: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: '5@s',
  },
  total: {
    fontSize: SIZES.h4,
    color: '#87898E',
    alignSelf: 'center',
  },
  iconMinus: {
    color: '#9B9B9B',
  },
  iconPlus: {
    color: COLORS.primaryColor,
  },
  priceView: {
    flexDirection: 'row',
    //marginTop:SIZES.base,
    borderBottomColor: COLORS.borderColor,
    borderBottomWidth: '0.8@s',
    paddingVertical: '18@vs',
  },
});
