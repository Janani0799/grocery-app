import React from 'react';
import {
  Text,
  SafeAreaView,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {FONTS, COLORS, adjust, Vector, Images, SIZES} from '../../constants';

export const CustomCard = ({
  title,
  image,
  onClick,
  ImageStyle,
  backgroundColor,
  borderColor,
}) => {
  return (
    <SafeAreaView style={styles.container}>
      <TouchableOpacity
        style={[
          styles.cardView,
          {backgroundColor: backgroundColor, borderColor: borderColor},
        ]}
        onPress={onClick}>
        <View>
          <Image
            source={{uri: image}}
            resizeMode={'contain'}
            style={ImageStyle}
          />
        </View>
        <View>
          <Text numberOfLines={2} style={styles.titletxt}>
            {title}
          </Text>
        </View>
      </TouchableOpacity>
    </SafeAreaView>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },

  cardView: {
    marginTop: '13@vs',
    borderRadius: '15@msr',
    width: '145@s',
    height: '155@vs',
    borderWidth: '0.8@s',
    bottom: 5,
    marginLeft: '12@s',
    paddingHorizontal: '20@s',
    alignItems: 'center',
  },

  image: {
    width: '83@s',
    height: '80@vs',
    marginHorizontal: '3@s',
    marginVertical: '3@vs',
    borderTopLeftRadius: '4@msr',
    borderBottomLeftRadius: '4@msr',
  },
  titletxt: {
    //...FONTS.P0,
    fontSize: adjust(14),
    fontWeight: '500',
    color: COLORS.blackColor,
    textAlign: 'center',
  },
  txt: {
    ...FONTS.body6,
    fontSize: adjust(10),

    color: COLORS.blackColor,
  },
  ButtonStyle: {
    height: '33@vs',
    width: '80@s',
    borderRadius: '5@msr',
    backgroundColor: COLORS.primaryColor,
  },
  buttonTextStyle: {
    ...FONTS.P0,
    color: COLORS.whiteColor,
  },
});
