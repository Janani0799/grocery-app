import React from 'react';
import {Text, View, Image, TouchableOpacity} from 'react-native';
import {FONTS, COLORS, adjust, Vector, Images} from '../../constants';
import {ScaledSheet} from 'react-native-size-matters';

const CommonMainCard = ({
  image,
  onClick,
  onClick2,
  title,
  price,
  quantity,
  cardStyle,
  ImageStyle,
  type,
  backgroundColor,
  onAdd,
}) => {
  return (
    <View style={styles.container}>
      {type == 1 ? (
        <TouchableOpacity
          style={styles.card2}
          onPress={onClick}
          activeOpacity={0.7}>
          <Image
            source={{uri: image}}
            resizeMode={'contain'}
            style={ImageStyle}
          />
          <View style={styles.bottomView}>
            <Text
              numberOfLines={1}
              style={[styles.cardText2, {textAlign: 'left'}]}>
              {title}
            </Text>

            <Text numberOfLines={1} style={styles.quantity}>
              {quantity}
            </Text>

            <View style={styles.priceView}>
              <View
                style={{
                  flex: 0.7,
                  alignItems: 'flex-start',
                  justifyContent: 'center',
                }}>
                <Text style={styles.price}>Rs. {price}</Text>
              </View>
              <TouchableOpacity style={{flex: 0.35}} onPress={onAdd}>
                <Image style={styles.plus} source={Images.plus} />
              </TouchableOpacity>
            </View>
          </View>
        </TouchableOpacity>
      ) : type == 2 ? (
        <TouchableOpacity
          style={[styles.card1, {backgroundColor: backgroundColor}]}
          onPress={onClick2}>
          <View style={styles.imageView}>
            <Image
              source={{uri: image}}
              resizeMode={'contain'}
              style={ImageStyle}
            />
          </View>
          <View style={{flex: 0.7}}>
            <Text style={styles.cardText1}>{title}</Text>
          </View>
        </TouchableOpacity>
      ) : null}
    </View>
  );
};

export default CommonMainCard;

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },
  bottomView: {
    padding: '6@vs',
  },
  cardText2: {
    //...FONTS.P0,
    fontSize: adjust(13),
    fontWeight: '500',
    color: COLORS.blackColor,
  },
  cardText1: {
    //...FONTS.P0,
    fontSize: adjust(14),
    fontWeight: '500',
    color: COLORS.blackColor,
    marginHorizontal: 15,
  },
  quantity: {
    ...FONTS.body4,
    fontSize: adjust(11),
    lineHeight: '18@vs',
    color: COLORS.textColor,
  },
  card2: {
    marginTop: '13@vs',
    borderRadius: '15@msr',
    width: '145@s',
    backgroundColor: COLORS.whiteColor,
    borderColor: COLORS.primaryColor,
    borderWidth: '1@s',
    bottom: 5,
    marginLeft: '12@s',
  },
  priceView: {
    flexDirection: 'row',
    paddingTop: '12@vs',
    paddingBottom: '5@vs',
  },
  plus: {
    height: '30@vs',
    width: '30@s',
    alignSelf: 'flex-end',
  },
  price: {
    //...FONTS.h5,
    fontWeight: '500',
    fontSize: adjust(13),
    color: COLORS.blackColor,
  },
  card1: {
    marginTop: '13@vs',
    borderRadius: '15@msr',
    width: '220@s',
    height: '88@vs',
    bottom: 5,
    marginLeft: '12@s',
    flexDirection: 'row',
    paddingHorizontal: '12@s',
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageView: {
    flex: 0.3,
  },
});
