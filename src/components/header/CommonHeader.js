import React from 'react';
import {
  View,
  SafeAreaView,
  StyleSheet,
  Image,
  Text,
  TouchableOpacity,
} from 'react-native';
import {COLORS, FONTS, adjust, Vector, Images} from '../../constants';
import {isIphoneX} from '../../utils';
import {ScaledSheet} from 'react-native-size-matters';
const CommonHeader = ({
  onClick,
  type,
  isShop,
  title,
  notifyNavigation,
  searchNavigation,
}) => {
  // const Defaultnavigation = useNavigation();
  return (
    <SafeAreaView style={Styles.container}>
      <View style={Styles.topView}>
        <View style={Styles.firstFlex}>
          <TouchableOpacity style={Styles.menu} onPress={onClick}>
            {type == 1 ? Vector.Account : Vector.Back}
          </TouchableOpacity>
        </View>

        {type == 1 ? (
          <View
            style={{alignItems: 'center', justifyContent: 'center', flex: 0.7}}>
            <Text style={Styles.headerTitle}>{title}</Text>
          </View>
        ) : (
          <View
            style={{
              alignItems: 'flex-start',
              justifyContent: 'center',
              flex: 0.95,
            }}>
            <Text style={Styles.headerTitle}>{title}</Text>
          </View>
        )}

        <View style={Styles.secondFlex}>
          {type == 1 ? (
            <TouchableOpacity style={Styles.notify} onPress={searchNavigation}>
              {Vector.Search}
            </TouchableOpacity>
          ) : null}
        </View>
      </View>
    </SafeAreaView>
  );
};
const Styles = ScaledSheet.create({
  container: {
    backgroundColor: COLORS.whiteColor,
    height: isIphoneX() ? '65@vs' : '60@vs',
  },
  topView: {
    flex: 1,
    flexDirection: 'row',
  },
  firstFlex: {
    flex: 0.2,
    alignItems: 'center',
    flexDirection: 'row',
  },
  secondFlex: {
    flex: 0.2,
    alignItems: 'center',
    justifyContent: 'flex-end',
    flexDirection: 'row',
  },
  menu: {marginLeft: '15@vs', alignItems: 'center'},
  notify: {marginRight: '15@vs', alignItems: 'center'},
  imageStyle: {
    height: '30@vs',
    width: '48@s',
    resizeMode: 'contain',
  },
  headerTitle: {
    //...FONTS.h4,
    fontSize: adjust(16),
    fontWeight: '500',
    color: COLORS.blackColor,
  },
});
export default CommonHeader;
