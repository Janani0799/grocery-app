import React, {useState, useEffect, useContext} from 'react';

import {
  Text,
  View,
  SectionList,
  SafeAreaView,
  Image,
  FlatList,
  TouchableOpacity,
  ScrollView,
  TextInput,
  ActivityIndicator,
} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import Icons from 'react-native-vector-icons/AntDesign';
import database from '@react-native-firebase/database';
import auth from '@react-native-firebase/auth';
import Snackbar from 'react-native-snackbar';
import {adjust, COLORS, FONTS, Images, SIZES, Vector} from '../../constants';
import {CommonMainCard} from '../../components';
//import {AuthContext} from '../../navigation/AuthProvider';
import {snackBar} from '../../utils';

const DATA = [
  {
    filename:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSU9xXfl2sD8KWJql4kn5JWi2wjy63PNDr2PA&usqp=CAU',
    title: 'Organic Bananas',
    price: 100,
    quantity: '7pcs',
    type: 1,
  },
  {
    filename:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRBJLRPheS9JlJytxuyTuvtRik1bhl5lG7ENIwpUo3tNrEkn_uMpdk8kI179slwQXJnu8M&usqp=CAU',
    title: 'Red Apple',
    price: 100,
    quantity: '7pcs',
    type: 1,
  },
  {
    filename:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSIEftk1FJm9bXeHw0pF_e8lh7H0-TJwQyZjA&usqp=CAU',
    title: 'Orange',
    price: 100,
    quantity: '7pcs',
    type: 1,
  },
  {
    filename:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSU9xXfl2sD8KWJql4kn5JWi2wjy63PNDr2PA&usqp=CA',
    title: 'Organic Bananas',
    price: 100,
    quantity: '7pcs',
    type: 1,
  },
  {
    filename:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRBJLRPheS9JlJytxuyTuvtRik1bhl5lG7ENIwpUo3tNrEkn_uMpdk8kI179slwQXJnu8M&usqp=CAU',
    title: 'Red Apple',
    price: 100,
    quantity: '7pcs',
    type: 1,
  },
  {
    filename:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSIEftk1FJm9bXeHw0pF_e8lh7H0-TJwQyZjA&usqp=CAU',
    title: 'Orange',
    price: 100,
    quantity: '7pcs',
    type: 1,
  },
  {
    filename:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSU9xXfl2sD8KWJql4kn5JWi2wjy63PNDr2PA&usqp=CAU',
    title: 'Organic Bananas',
    price: 100,
    quantity: '7pcs',
    type: 1,
  },
  {
    filename:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRBJLRPheS9JlJytxuyTuvtRik1bhl5lG7ENIwpUo3tNrEkn_uMpdk8kI179slwQXJnu8M&usqp=CAU',
    title: 'Red Apple',
    price: 100,
    quantity: '7pcs',
    type: 1,
  },
  {
    filename:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSIEftk1FJm9bXeHw0pF_e8lh7H0-TJwQyZjA&usqp=CAU',
    title: 'Orange',
    price: 100,
    quantity: '7pcs',
    type: 1,
  },
  {
    filename:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSU9xXfl2sD8KWJql4kn5JWi2wjy63PNDr2PA&usqp=CAU',
    title: 'Organic Bananas',
    price: 100,
    quantity: '7pcs',
    type: 1,
  },
];

const Search = ({navigation}) => {
  const handleInput = () => {
    setArray({array: ''});
    setIcon(false);
    setFilteredData(dataSource);
  };

  const handleSearch = array => {
    if (array.length !== 0) {
      setIcon(true);
    } else {
      setIcon(false);
    }
  };

  const searchFunction = array => {
    if (array) {
      const newData = dataSource.filter(function (item) {
        const itemData = item.title
          ? item.title.toUpperCase()
          : ''.toUpperCase();
        const textData = array.toUpperCase();
        return itemData.indexOf(textData) > -1;
      });
      setFilteredData(newData);
      setArray(array);
    } else {
      setFilteredData(dataSource);
      setArray(array);
    }
  };

  const twoFunction = array => {
    setArray(array);
    searchFunction(array);
    handleSearch(array);
    console.log('array', array);
  };

  const [array, setArray] = React.useState([]);

  const [icon, setIcon] = React.useState(false);

  const [filteredData, setFilteredData] = React.useState(DATA);

  const [dataSource, setDataSource] = React.useState(DATA);

  const [datas, setDatas] = useState([]);
  //const [loading, setLoading] = useState(true);

  //const {user, logout} = useContext(AuthContext);

  const addAction = (item, index) => {
    cartItem.push(item);
    Snackbar.show({
      text: 'Item added to cart !',
      duration: Snackbar.LENGTH_LONG,
      textColor: COLORS.whiteColor,
      backgroundColor: COLORS.primaryColor,
      action: {
        text: 'View cart',
        textColor: COLORS.whiteColor,
        onPress: () => {navigation.navigate("CartStackScreen")},
      },
    })
    //snackBar('Item added to cart !');
  };

  let cartItem = database().ref(`/users/${auth().currentUser.uid}/cart`);

  const ListItem = ({item, index}) => {
    return (
      <CommonMainCard
        image={item.filename}
        title={item.title}
        type={item.type}
        price={item.price}
        quantity={item.quantity}
        onAdd={() => addAction(item, index)}
        onClick={() =>
          navigation.navigate('ProductDetails', {
            passItem: item,
            PassImage: item.filename,
            PassText: item.title,
            PassPrice: item.price,
            PassQuantity: item.quantity,
          })
        }
        ImageStyle={styles.img}
      />
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <View
        style={{
          height: SIZES.height * 0.12,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <TextInput
          style={styles.input}
          placeholder="Search for your product..."
          placeholderTextColor={COLORS.textColor}
          onChangeText={twoFunction}
          value={array}
        />
        {icon ? (
          <TouchableOpacity
            style={styles.icon}
            onPress={handleInput}
            activeOpacity={0.6}>
            <Icons name="closecircleo" size={18} color={COLORS.darkGrey} />
          </TouchableOpacity>
        ) : null}
      </View>

      <View style={{justifyContent: 'center'}}>
        <FlatList
          contentContainerStyle={styles.flatListSyle}
          data={filteredData}
          renderItem={ListItem}
          numColumns={'2'}
          showsVerticalScrollIndicator={false}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    </SafeAreaView>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.whiteColor,
  },
  img: {
    height: '80@vs',
    width: '80@s',
    alignSelf: 'center',
  },
  flatListSyle: {
    //paddingTop: "15@vs",
    paddingBottom: '85@vs',
    paddingHorizontal: '8@s',
  },
  input: {
    backgroundColor: '#F2F3F2',
    color: COLORS.blackColor,
    borderRadius: '12@msr',
    height: '45@vs',
    position: 'relative',
    //marginLeft: "10@msr",
    fontSize: adjust(11),
    paddingLeft: '12@msr',
    paddingRight: '38@msr',
    width: SIZES.width * 0.9,
  },
  icon: {
    position: 'absolute',
    right: '28@s',
  },
});

export default Search;
