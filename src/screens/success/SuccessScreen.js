import React from 'react';
import {Text, SafeAreaView, View, Image} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {FONTS, COLORS, adjust, Vector, Images, SIZES} from '../../constants';
import {CommonButton} from '../../components';

const SuccessScreen = ({navigation}) => {
  return (
    <SafeAreaView style={styles.container}>
      <View>
        <Image
          style={styles.image}
          resizeMode={'contain'}
          source={Images.success}
        />
      </View>
      <View style={styles.txtView}>
        <Text style={styles.title}>Your order has been {'\n'} accepted</Text>
        <Text style={styles.text}>
          Your items has been placed and is on {'\n'} it’s way to being
          processed
        </Text>
      </View>
      <View style={styles.buttonView}>
        <CommonButton
          buttonColor={COLORS.primaryColor}
          text={'Back to Home'}
          width={SIZES.width * 0.9}
          onPress={() => navigation.navigate('HomeScreen')}
        />
      </View>
    </SafeAreaView>
  );
};

export default SuccessScreen;

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.whiteColor,
  },
  image: {
    height: '210@vs',
    width: '220@vs',
    marginTop: '100@vs',
    marginHorizontal: '45@s',
  },
  txtView: {
    paddingVertical: '20@vs',
    alignItems: 'center',
  },
  title: {
    textAlign: 'center',
    lineHeight: '23@vs',
    ...FONTS.h3,
    color: COLORS.blackColor,
  },
  text: {
    textAlign: 'center',
    lineHeight: '18@vs',
    ...FONTS.h5,
    color: COLORS.textColor,
    marginVertical: '18@vs',
  },
  buttonView: {
    alignItems: 'center',
    marginTop: '85@vs',
  },
});
