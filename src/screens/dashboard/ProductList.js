import React, {useState, useEffect, useContext} from 'react';
import {
  Text,
  View,
  SectionList,
  SafeAreaView,
  Image,
  FlatList,
  TouchableOpacity,
  ScrollView,
  ActivityIndicator,
} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import database from '@react-native-firebase/database';
import auth from '@react-native-firebase/auth';
import Snackbar from 'react-native-snackbar';
import {adjust, COLORS, FONTS, Images} from '../../constants';
import {CommonMainCard} from '../../components';
//import {AuthContext} from '../../navigation/AuthProvider';
import {snackBar} from '../../utils';

const ProductList = ({navigation}) => {
  const [datas, setDatas] = useState([]);
  const [loading, setLoading] = useState(true);

  const [cartData, setCartData] = useState([]);

  useEffect(() => {
    itemsRef.on('value', snapshot => {
      let data = snapshot.val();
      const products = Object.values(data);
      setDatas(products);
      setLoading(false);
    });
  }, []);

  let itemsRef = database().ref(`/sections/0/data`);
  console.log('section', datas);

 // const {user} = useContext(AuthContext);

  const addAction = (item, index) => {
    cartItem.push(item);
    Snackbar.show({
      text: 'Item added to cart !',
      duration: Snackbar.LENGTH_LONG,
      textColor: COLORS.whiteColor,
      backgroundColor: COLORS.primaryColor,
      action: {
        text: 'View cart',
        textColor: COLORS.whiteColor,
        onPress: () => {navigation.navigate("CartStackScreen")},
      },
    })
    //snackBar('Item added to cart !');
  };

  let cartItem = database().ref(`/users/${auth().currentUser.uid}/cart`);

  const ListItem = ({item, index}) => {
    return (
      <CommonMainCard
        image={item.filename}
        title={item.title}
        type={item.type}
        price={item.price}
        quantity={item.quantity}
        onAdd={() => addAction(item, index)}
        onClick={() =>
          navigation.navigate('ProductDetails', {
            passItem: item,
            PassImage: item.filename,
            PassText: item.title,
            PassPrice: item.price,
            PassQuantity: item.quantity,
          })
        }
        ImageStyle={styles.img}
      />
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      {loading ? (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <ActivityIndicator size="large" color={COLORS.primaryColor} />
        </View>
      ) : (
        <View style={{paddingHorizontal: 10}}>
          <FlatList
            contentContainerStyle={styles.flatListSyle}
            data={datas}
            renderItem={ListItem}
            numColumns={'2'}
            showsVerticalScrollIndicator={false}
          />
        </View>
      )}
    </SafeAreaView>
  );
};

export default ProductList;

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.whiteColor,
  },
  img: {
    height: '80@vs',
    width: '80@s',
    alignSelf: 'center',
  },
  flatListSyle: {
    paddingTop: '15@vs',
    paddingBottom: '20@vs',
  },
});
