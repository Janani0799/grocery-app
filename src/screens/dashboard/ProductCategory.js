import React from 'react';

import {
  Text,
  View,
  SectionList,
  SafeAreaView,
  Image,
  FlatList,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {adjust, COLORS, FONTS, Images} from '../../constants';
import {CustomCard} from '../../components';

const DATA = [
  {
    image: 'https://pngimage.net/wp-content/uploads/2018/06/pulses-png-8.png',
    text: 'Bakery & Snacks',
    // price: "Rs. 100 ",
    type: 2,
    backgroundColor: 'rgba(248, 164, 76, 0.25)',
    borderColor: 'rgba(248, 164, 76, 1)',
  },
  {
    image: 'https://pngimage.net/wp-content/uploads/2018/06/pulses-png-8.png',
    text: 'Rice',
    //price: "Rs. 100 ",
    type: 2,
    backgroundColor: 'rgba(83, 177, 117, 0.25)',
    borderColor: 'rgba(83, 177, 117, 1)',
  },
  {
    image: 'https://pngimage.net/wp-content/uploads/2018/06/pulses-png-8.png',
    text: 'Snacks',
    //price: "Rs. 100 ",
    type: 2,
    backgroundColor: 'rgba(247, 165, 147, 0.25)',
    borderColor: ' rgba(247, 165, 147, 1)',
  },
  {
    image: 'https://pngimage.net/wp-content/uploads/2018/06/pulses-png-8.png',
    text: 'Fresh Fruits & Vegetables',
    //price: "Rs. 100 ",
    type: 2,
    backgroundColor: 'rgba(211, 176, 224, 0.25)',
    borderColor: 'rgba(211, 176, 224, 1)',
  },
  {
    image: 'https://pngimage.net/wp-content/uploads/2018/06/pulses-png-8.png',
    text: 'Red Apple',
    //price: "Rs. 100 ",
    type: 2,
    backgroundColor: 'rgba(248, 164, 76, 0.25)',
    borderColor: 'rgba(248, 164, 76, 1)',
  },
  {
    image: 'https://pngimage.net/wp-content/uploads/2018/06/pulses-png-8.png',
    text: 'Snacks',
    //price: "Rs. 100 ",
    type: 2,
    backgroundColor: 'rgba(83, 177, 117, 0.25)',
    borderColor: 'rgba(83, 177, 117, 1)',
  },
  {
    image: 'https://pngimage.net/wp-content/uploads/2018/06/pulses-png-8.png',
    text: 'Organic Bananas',
    //price: "Rs. 100 ",
    type: 2,
    backgroundColor: 'rgba(247, 165, 147, 0.25)',
    borderColor: ' rgba(247, 165, 147, 1)',
  },
  {
    image: 'https://pngimage.net/wp-content/uploads/2018/06/pulses-png-8.png',
    text: 'Red Apple',
    //price: "Rs. 100 ",
    type: 2,
    backgroundColor: 'rgba(211, 176, 224, 0.25)',
    borderColor: 'rgba(211, 176, 224, 1)',
  },
  {
    image: 'https://pngimage.net/wp-content/uploads/2018/06/pulses-png-8.png',
    text: 'Snacks',
    //price: "Rs. 100 ",
    type: 2,
    backgroundColor: 'rgba(248, 164, 76, 0.25)',
    borderColor: 'rgba(248, 164, 76, 1)',
  },
  {
    image: 'https://pngimage.net/wp-content/uploads/2018/06/pulses-png-8.png',
    text: 'Organic Bananas',
    //price: "Rs. 100 ",
    type: 2,
    backgroundColor: 'rgba(83, 177, 117, 0.25)',
    borderColor: 'rgba(83, 177, 117, 1)',
  },
];

const ProductCategory = ({navigation}) => {
  const ListItem = ({item}) => {
    return (
      <CustomCard
        image={item.image}
        title={item.text}
        onClick={() => navigation.navigate('CategoryList')}
        ImageStyle={styles.img2}
        backgroundColor={item.backgroundColor}
        borderColor={item.borderColor}
      />
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={{paddingHorizontal: 10}}>
        <FlatList
          contentContainerStyle={styles.flatListSyle}
          data={DATA}
          renderItem={ListItem}
          numColumns={'2'}
          showsVerticalScrollIndicator={false}
        />
      </View>
    </SafeAreaView>
  );
};

export default ProductCategory;

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.whiteColor,
  },
  img2: {
    height: '60@vs',
    width: '60@s',
    //alignSelf:'center',
    marginVertical: '20@vs',
  },
  flatListSyle: {
    paddingTop: '15@vs',
    paddingBottom: '20@vs',
  },
});
