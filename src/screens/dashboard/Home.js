import React, {useState, useEffect, useContext} from 'react';
import {
  Text,
  View,
  SectionList,
  SafeAreaView,
  Image,
  FlatList,
  TouchableOpacity,
  ScrollView,
  ActivityIndicator,
} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import database from '@react-native-firebase/database';
import auth from '@react-native-firebase/auth';
import Snackbar from 'react-native-snackbar';
import {adjust, COLORS, FONTS, Images} from '../../constants';
import {CommonMainCard} from '../../components';
//import {AuthContext} from '../../navigation/AuthProvider';
import {snackBar} from '../../utils';


const Home = ({navigation}) => {
  const [datas, setDatas] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    itemsRef.on('value', snapshot => {
      let data = snapshot.val();
      const products = Object.values(data);
      setDatas(products);
      setLoading(false);
    });
  }, []);

  let itemsRef = database().ref(`/sections`);

  //const {user, logout} = useContext(AuthContext);

  const addAction = (item, index) => {
    cartItem.push(item);
    Snackbar.show({
      text: 'Item added to cart !',
      duration: Snackbar.LENGTH_LONG,
      textColor: COLORS.whiteColor,
      backgroundColor: COLORS.primaryColor,
      action: {
        text: 'View cart',
        textColor: COLORS.whiteColor,
        onPress: () => {navigation.navigate("CartStackScreen")},
      },
    })
   // snackBar('Item added to cart !');
  };

  let cartItem = database().ref(`/users/${auth().currentUser.uid}/cart`);

  const ListItem = ({item, index}) => {
    return (
      <CommonMainCard
        image={item.filename}
        title={item.title}
        type={item.type}
        price={item.price}
        quantity={item.quantity}
        onAdd={() => addAction(item, index)}
        onClick2={() => navigation.navigate('CategoryList')}
        onClick={() =>
          navigation.navigate('ProductDetails', {
            passItem: item,
            PassImage: item.filename,
            PassText: item.title,
            PassPrice: item.price,
            PassQuantity: item.quantity,
          })
        }
        ImageStyle={item.type == 1 ? styles.img : styles.img2}
        backgroundColor={item.backgroundColor}
      />
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      {loading ? (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <ActivityIndicator size="large" color={COLORS.primaryColor} />
        </View>
      ) : (
        <SectionList
          contentContainerStyle={styles.sectionHeight}
          stickySectionHeadersEnabled={false}
          sections={datas}
          renderSectionHeader={({section}) => (
            <>
              <View style={styles.titleView}>
                <View style={styles.titleView1}>
                  <Text numberOfLines={1} style={styles.sectionHeader}>
                    {section.title}
                  </Text>
                </View>
                <View style={styles.titleView2}>
                  <TouchableOpacity
                    onPress={() => {
                      section.types == 1
                        ? navigation.navigate('ProductList')
                        : navigation.navigate('ProductCategory');
                    }}>
                    <Text numberOfLines={1} style={styles.viewMoreTxt}>
                      {'See all'}
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
              {section.horizontal ? (
                <FlatList
                  horizontal
                  data={section.data}
                  renderItem={({item}) => {
                    return (
                      <>
                        <ListItem item={item} />
                      </>
                    );
                  }}
                  showsHorizontalScrollIndicator={false}
                />
              ) : null}
            </>
          )}
          renderItem={({item, section}) => {
            if (section.horizontal) {
              return null;
            }
            return <ListItem item={item} />;
          }}
        />
      )}
    </SafeAreaView>
  );
};

export default Home;

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.whiteColor,
  },
  titleView: {
    flexDirection: 'row',
    paddingHorizontal: '12@s',
    height: '30@vs',
    marginTop: '10@vs',
  },
  sectionHeader: {
    //...FONTS.h5,
    fontSize: adjust(15),
    fontWeight: '500',
    color: COLORS.blackColor,
  },
  viewMoreTxt: {
    ...FONTS.t0,
    //fontSize: adjust(10),
    color: COLORS.primaryColor,
  },
  sectionHeight: {paddingBottom: '60@vs'},
  card2: {
    marginTop: '13@vs',
    borderRadius: '15@msr',
    width: '145@s',
    backgroundColor: COLORS.whiteColor,
    borderColor: COLORS.primaryColor,
    borderWidth: '1@s',
    bottom: 5,
    marginLeft: '10@vs',
  },
  img: {
    height: '80@vs',
    width: '80@s',
    alignSelf: 'center',
  },
  img2: {
    height: '62@vs',
    width: '62@s',
    alignSelf: 'center',
  },

  titleView1: {flex: 0.75, justifyContent: 'center'},
  titleView2: {flex: 0.25, alignItems: 'flex-end', justifyContent: 'center'},
});
