import React, {useContext, useState} from 'react';
import {
  Text,
  View,
  SectionList,
  SafeAreaView,
  Image,
  FlatList,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import Icons from 'react-native-vector-icons/AntDesign';
import Snackbar from 'react-native-snackbar';
import {adjust, COLORS, FONTS, SIZES, Images, Vector} from '../../constants';
import {CommonButton} from '../../components';
import database from '@react-native-firebase/database';
import auth from '@react-native-firebase/auth';
//import {AuthContext} from '../../navigation/AuthProvider';
import {snackBar} from '../../utils';

const ProductDetails = ({route, navigation}) => {
  const {PassImage, PassText, PassPrice, PassQuantity, passItem} = route.params;

  const [total, setTotal] = React.useState(1);
  const increase = () => {
    setTotal(total + 1);
  };
  let decrease = () => {
    setTotal(total - 1);
  };
  if (total <= 1) {
    decrease = () => setTotal(1);
  }

  //const {user} = useContext(AuthContext);

  const [isFav, setIsFav] = useState(false);

  const amount = total * PassPrice;

  const addAction = () => {
    cartItem.push({
      filename: PassImage,
      title: PassText,
      quantity: PassQuantity,
      price: amount,
      type: 1,
    });
    Snackbar.show({
      text: 'Item added to cart !',
      duration: Snackbar.LENGTH_LONG,
      textColor: COLORS.whiteColor,
      backgroundColor: COLORS.primaryColor,
      action: {
        text: 'View cart',
        textColor: COLORS.whiteColor,
        onPress: () => {navigation.navigate("CartStackScreen")},
      },
    })
    //snackBar('Item added to cart !');
  };

  let cartItem = database().ref(`/users/${auth().currentUser.uid}/cart`);

  const favAction = () => {
    favItem.push(passItem);
    setIsFav(true);
    Snackbar.show({
      text: 'Item added to favourites !',
      duration: Snackbar.LENGTH_LONG,
      textColor: COLORS.whiteColor,
      backgroundColor: COLORS.primaryColor,
      action: {
        text: 'View favourites',
        textColor: COLORS.whiteColor,
        onPress: () => {navigation.navigate("FavouritesStackScreen")},
      },
    })
    //snackBar('Item added to favourites !');
  };

  let favItem = database().ref(`/users/${auth().currentUser.uid}/favourites`);

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.topView}>
        <Image
          source={{uri: PassImage}}
          resizeMode={'contain'}
          style={styles.image}
        />
      </View>
      <View style={styles.bottomView}>
        <View style={{flexDirection: 'row'}}>
          <View style={{flex: 0.7}}>
            <Text style={styles.title}>{PassText}</Text>
          </View>
          <TouchableOpacity
            style={{flex: 0.7, alignItems: 'flex-end'}}
            onPress={() => favAction()}>
            {isFav ? Vector.Heart : Vector.HeartO}
          </TouchableOpacity>
        </View>
        <Text style={styles.quantity}>{PassQuantity}</Text>
        <View style={styles.priceView}>
          <View style={{flex: 0.7}}>
            <View style={styles.ButtonStyle}>
              <View style={styles.button}>
                <View style={{justifyContent: 'center'}}>
                  <TouchableOpacity onPress={() => decrease()}>
                    <Icons name="minus" size={20} style={styles.iconMinus} />
                  </TouchableOpacity>
                </View>
                <View style={{justifyContent: 'center'}}>
                  <Text style={styles.total}>{total}</Text>
                </View>
                <View style={{justifyContent: 'center'}}>
                  <TouchableOpacity onPress={() => increase()}>
                    <Icons name="plus" style={styles.iconPlus} size={20} />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>

          <View
            style={{
              flex: 0.35,
              alignItems: 'flex-end',
              marginTop: SIZES.base - 5,
            }}>
            <Text style={styles.title}>Rs. {amount}</Text>
          </View>
        </View>

        <View style={styles.productDetail}>
          <Text style={styles.productTitle}>{'Product Detail'}</Text>
          <Text style={styles.productText} numberOfLines={5}>
            {
              'Apples are nutritious. Apples may be good for weight loss. apples may be good for your heart. As part of a healtful and varied diet. '
            }
          </Text>
        </View>
        <View style={{marginTop: SIZES.base * 2, alignItems: 'center'}}>
          <CommonButton
            buttonColor={COLORS.primaryColor}
            text={'Add To cart'}
            width={SIZES.width * 0.9}
            onPress={() => addAction()}
          />
        </View>
      </View>
    </SafeAreaView>
  );
};

export default ProductDetails;

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F2F3F2',
  },
  topView: {
    flex: 0.38,
    backgroundColor: COLORS.whiteColor,
    borderBottomLeftRadius: '25@msr',
    borderBottomRightRadius: '25@msr',
    justifyContent: 'center',
    alignItems: 'center',
  },
  bottomView: {
    flex: 0.6,
    paddingVertical: '20@vs',
    paddingHorizontal: '15@vs',
  },
  image: {
    height: '225@vs',
    width: '225@s',
  },
  title: {
    //...FONTS.P0,
    fontSize: adjust(16),
    fontWeight: '500',
    color: COLORS.blackColor,
  },
  quantity: {
    ...FONTS.body4,
    fontSize: adjust(13),
    lineHeight: '20@vs',
    color: COLORS.textColor,
  },
  ButtonStyle: {
    height: '32@vs',
    width: '87@s',
    borderRadius: '5@msr',
    backgroundColor: COLORS.whiteColor,
    justifyContent: 'center',
    alignSelf: 'flex-start',
  },

  button: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: '5@s',
  },
  total: {
    fontSize: SIZES.h4,
    color: '#87898E',
    alignSelf: 'center',
  },
  iconMinus: {
    //fontSize: SIZES.P1,

    color: '#9B9B9B',
  },
  iconPlus: {
    //fontSize: SIZES.P1,

    color: COLORS.primaryColor,
  },
  priceView: {
    flexDirection: 'row',
    //marginTop:SIZES.base,
    borderBottomColor: COLORS.borderColor,
    borderBottomWidth: '0.8@s',
    paddingVertical: '18@vs',
  },
  productTitle: {
    //...FONTS.P0,
    fontSize: adjust(13),
    fontWeight: '500',
    color: COLORS.blackColor,
    lineHeight: '20@vs',
  },
  productText: {
    ...FONTS.body4,
    color: COLORS.textColor,
    fontSize: adjust(13),
    paddingVertical: '10@vs',
    lineHeight: '18@vs',
  },
  productDetail: {
    paddingVertical: '15@vs',
    height: SIZES.height * 0.25,
  },
});
