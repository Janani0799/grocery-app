import React, {useEffect, useState} from 'react';
import {SafeAreaView, StyleSheet, View, ActivityIndicator} from 'react-native';
import {WebView} from 'react-native-webview';
import {COLORS} from '../../constants';

const PrivacyPolicy = () => {
  useEffect(() => {
    setTimeout(() => {
      setLoading(false);
    }, 600);
  }, []);

  const [loading, setLoading] = useState(true);

  return (
    <SafeAreaView style={{flex: 1}}>
      {loading ? (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <ActivityIndicator size="large" color={COLORS.primaryColor} />
        </View>
      ) : (
        <View style={styles.container}>
          <WebView
            source={{
              uri: 'https://www.privacypolicygenerator.info/live.php?token=GxYmTsVyMeKi3GZavQl165Ehxwi4wBuw',
            }}
            hasZoom={false}
            scrollEnabled={false}
          />
        </View>
      )}
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F5FCFF',
    flex: 1,
  },
});
export default PrivacyPolicy;
