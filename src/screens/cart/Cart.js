import {
  Text,
  SafeAreaView,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  FlatList,
  ActivityIndicator,
  Alert,
} from 'react-native';
import React, {useState, useEffect, useContext} from 'react';
import {ScaledSheet} from 'react-native-size-matters';
import Icons from 'react-native-vector-icons/AntDesign';
import database from '@react-native-firebase/database';
import auth from '@react-native-firebase/auth';
import Mailer from 'react-native-mail';
import {FONTS, COLORS, adjust, Vector, Images, SIZES} from '../../constants';
import {CommonButton} from '../../components';
//import {AuthContext} from '../../navigation/AuthProvider';
import {CommonPopup} from '../../components';

const Cart = ({navigation}) => {
  const [visible, setVisible] = useState(false);
  const [total, setTotal] = React.useState(0);
  const increase = () => {
    setTotal(total + 1);
  };
  let decrease = () => {
    setTotal(total - 1);
  };
  if (total <= 0) {
    decrease = () => setTotal(0);
  }
  //const {user} = useContext(AuthContext);

  const [datas, setDatas] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    itemsRef.on('value', snapshot => {
      let data = snapshot.val();
      if (data !== null) {
        const products = Object.values(data);
        setDatas(products);
        // setLoading(false);
        //console.log(products);
      } else {
      }
      setLoading(false);
    });
    // setLoading(false);
    //console.log('datas', datas);
  }, []);

  let itemsRef = database().ref(`/users/${auth().currentUser.uid}/cart`);

  const onRemove = (item, index) => {
    console.log('index', index);
    const data = datas;
    data.splice(index, 1);
    setDatas([...data]);
  };

  const sum = datas
    .map(item => item.price)
    .reduce((prev, curr) => prev + curr, 0);

  const sumFunc = () => {
    setVisible(!visible);
  };

  const name = datas.map(item => item.title);

  const price = datas.map(item => item.price);

  const handleEmail = () => {
    Mailer.mail(
      {
        subject: 'Grocery App - Amount',
        recipients: ['jananiiisiam@gmail.com'],
        body: `<h3>Price summary:</b></h3>
      <p> ${name}   -  ${price} </p>
      <p>Total amount to be paid Rs: ${sum}</p>`,
        isHTML: true,
        customChooserTitle: 'Grocery App - Amount',
      },
      (error, event) => {
        Alert.alert(
          error,
          event,
          [
            {
              text: 'Ok',
              onPress: () => console.log('OK: Email Error Response'),
            },
            {
              text: 'Cancel',
              onPress: () => console.log('CANCEL: Email Error Response'),
            },
          ],
          {cancelable: true},
        );
      },
    );
  };

  const yesFunc = () => {
    database().ref(`/users/${auth().currentUser.uid}`).child('cart').set('');
    setVisible(!visible);
    handleEmail();
    navigation.navigate('SuccessScreen');
  };

  const ListItem = ({item, index}) => {
    //console.log('cartitem', item);
    return (
      <View
        style={{
          flexDirection: 'row',
          height: SIZES.height * 0.18,
          borderBottomWidth: 1,
          borderBottomColor: COLORS.borderColor,
        }}>
        <View style={{flex: 0.3, justifyContent: 'center'}}>
          <Image
            source={{uri: item.filename}}
            resizeMode={'contain'}
            style={styles.img}
          />
        </View>
        <View style={{flex: 0.5, paddingHorizontal: 15, paddingVertical: 20}}>
          <Text style={styles.title} numberOfLines={1}>
            {item.title}
          </Text>
          <Text numberOfLines={1} style={styles.quantity}>
            {item.quantity}
          </Text>

          <View style={styles.ButtonStyle}>
            <View>
              <Text style={styles.price}>Rs. {item.price}</Text>
            </View>
            {/* <View style={styles.button}>
              <View style={{justifyContent: 'center'}}>
                <TouchableOpacity onPress={() => decrease(item, index)}>
                  <Icons name="minus" size={20} style={styles.iconMinus} />
                </TouchableOpacity>
              </View>
              <View style={{justifyContent: 'center'}}>
                <Text style={styles.total}>{total}</Text>
              </View>
              <View style={{justifyContent: 'center'}}>
                <TouchableOpacity onPress={() => increase(item, index)}>
                  <Icons name="plus" style={styles.iconPlus} size={20} />
                </TouchableOpacity>
              </View>
            </View> */}
          </View>
        </View>

        <View style={{flex: 0.35, alignItems: 'flex-end'}}>
          <TouchableOpacity
            style={{paddingHorizontal: 12, paddingVertical: 15}}
            onPress={() => onRemove(item, index)}>
            {Vector.CancelX}
          </TouchableOpacity>
          {/* <View style={{paddingTop: 25, paddingHorizontal: 10}}>
            <Text style={styles.price}>Rs. {item.price}</Text>
          </View> */}
        </View>
      </View>
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      {loading ? (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <ActivityIndicator size="large" color={COLORS.primaryColor} />
        </View>
      ) : (
        <View>
          {datas.length !== 0 ? (
            <View style={{paddingHorizontal: 10}}>
              <FlatList
                contentContainerStyle={styles.flatListSyle}
                data={datas}
                renderItem={ListItem}
                showsVerticalScrollIndicator={false}
              />
              <View style={styles.buttonView}>
                <CommonButton
                  buttonColor={COLORS.primaryColor}
                  text={'Go to Checkout'}
                  width={SIZES.width * 0.9}
                  onPress={sumFunc}
                />
                <View style={styles.totalView}>
                  <Text style={styles.sumTxt}>Rs. {sum}</Text>
                </View>
              </View>
            </View>
          ) : (
            <View>
              <View style={{justifyContent: 'center', alignItems: 'center'}}>
                <Image
                  style={styles.image}
                  resizeMode={'contain'}
                  source={Images.emptyCart}
                />
              </View>
              <View style={styles.txtView}>
                <Text style={styles.emptyTitle}>Your cart is empty !</Text>
                <Text style={styles.emptyText}>
                  Looks like you haven’t added any items {'\n'} to your cart yet
                </Text>
              </View>
              <View style={styles.buttonView2}>
                <CommonButton
                  buttonColor={COLORS.primaryColor}
                  text={'Add items to cart'}
                  width={SIZES.width * 0.9}
                  onPress={() => navigation.navigate('HomeScreen')}
                />
              </View>
            </View>
          )}
        </View>
      )}

      <CommonPopup
        title={'Do You Want to place this order ?'}
        subtitle={'Total amount to be paid :'}
        text={sum}
        type={1}
        isVisible={visible}
        successButtonText={'Yes'}
        negativeButtonText={'No'}
        onPressNo={() => setVisible(!visible)}
        onPressYes={yesFunc}
      />
    </SafeAreaView>
  );
};

export default Cart;

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.whiteColor,
  },
  ButtonStyle: {
    marginTop: '10@vs',
    height: '32@vs',
    width: '87@s',
    borderRadius: '5@msr',
    backgroundColor: COLORS.whiteColor,
    justifyContent: 'center',
    alignSelf: 'flex-start',
  },

  button: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: '5@s',
  },
  total: {
    fontSize: SIZES.h4,
    color: '#87898E',
    alignSelf: 'center',
  },
  iconMinus: {
    color: '#9B9B9B',
  },
  iconPlus: {
    color: COLORS.primaryColor,
  },
  priceView: {
    flexDirection: 'row',
    //marginTop:SIZES.base,
    borderBottomColor: COLORS.borderColor,
    borderBottomWidth: '0.8@s',
    paddingVertical: '18@vs',
  },
  productTitle: {
    //...FONTS.P0,
    fontSize: adjust(13),
    fontWeight: '500',
    color: COLORS.blackColor,
    lineHeight: '20@vs',
  },

  flatListSyle: {
    paddingTop: '15@vs',
    paddingBottom: '140@vs',
  },
  img: {
    height: '80@vs',
    width: '80@s',
    alignSelf: 'center',
  },
  title: {
    fontSize: adjust(14),
    fontWeight: '500',
    color: COLORS.blackColor,
    lineHeight: '20@vs',
  },
  quantity: {
    ...FONTS.body4,
    fontSize: adjust(12),
    lineHeight: '18@vs',
    color: COLORS.textColor,
  },
  price: {
    //...FONTS.P0,
    fontSize: adjust(15),
    fontWeight: '500',
    color: COLORS.blackColor,
  },
  buttonView: {
    alignItems: 'center',
    paddingTop: '15@vs',
    paddingBottom: '80@vs',
    position: 'absolute',
    top: '480@vs',
    right: '10@s',
    left: '10@s',
  },
  totalView: {
    position: 'absolute',
    top: '27@vs',
    left: '235@s',
    zIndex: 1,
    backgroundColor: COLORS.whiteColor,
    paddingVertical: '3@vs',
    paddingHorizontal: '5@vs',
    borderRadius: '5@msr',
    //height:SIZES.height * 0.07
  },
  sumTxt: {
    color: COLORS.primaryColor,
    ...FONTS.P0,
  },

  //empty cart

  image: {
    height: '280@vs',
    width: '320@vs',
  },
  txtView: {
    paddingVertical: '15@vs',
    alignItems: 'center',
  },
  emptyTitle: {
    textAlign: 'center',
    lineHeight: '23@vs',
    ...FONTS.h3,
    color: COLORS.blackColor,
  },
  emptyText: {
    textAlign: 'center',
    lineHeight: '18@vs',
    ...FONTS.h5,
    color: COLORS.textColor,
    marginVertical: '18@vs',
  },
  buttonView2: {
    alignItems: 'center',
    marginTop: '65@vs',
  },
});
