import {
  Text,
  SafeAreaView,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  FlatList,
  ActivityIndicator,
  Alert,
} from 'react-native';
import React, {useState, useEffect, useContext} from 'react';
import {ScaledSheet} from 'react-native-size-matters';
import database from '@react-native-firebase/database';
import auth from '@react-native-firebase/auth';
import Mailer from 'react-native-mail';
import {FONTS, COLORS, adjust, Vector, Images, SIZES} from '../../constants';
import {CommonButton, CommonPopup} from '../../components';
//import {AuthContext} from '../../navigation/AuthProvider';

const Favourites = ({navigation}) => {
  //const {user} = useContext(AuthContext);

  const [datas, setDatas] = useState([]);
  const [loading, setLoading] = useState(true);
  const [visible, setVisible] = useState(false);

  useEffect(() => {
    itemsRef.on('value', snapshot => {
      let data = snapshot.val();
      if (data !== null) {
        const products = Object.values(data);
        setDatas(products);
        //setLoading(false);
        console.log(products);
      } else {
      }
      setLoading(false);
    });
    //setLoading(false);
    console.log('datas', datas);
  }, []);

  let itemsRef = database().ref(`/users/${auth().currentUser.uid}/favourites`);

  const sum = datas
    .map(item => item.price)
    .reduce((prev, curr) => prev + curr, 0);

  const onRemove = () => {
    database().ref(`/users/${auth().currentUser.uid}`).child('favourites').set('');
  };

  const onBuy = () => {
    setVisible(!visible);
  };

  const name = datas.map(item => item.title);

  const price = datas.map(item => item.price);

  const handleEmail = () => {
    Mailer.mail(
      {
        subject: 'Grocery App - Amount',
        recipients: ['jananiiisiam@gmail.com'],
        body: `<h3>Price summary:</b></h3>
      <p> ${name}   -  ${price} </p>
      <p>Total amount to be paid Rs: ${sum}</p>`,
        isHTML: true,
        customChooserTitle: 'Grocery App - Amount',
      },
      (error, event) => {
        Alert.alert(
          error,
          event,
          [
            {
              text: 'Ok',
              onPress: () => console.log('OK: Email Error Response'),
            },
            {
              text: 'Cancel',
              onPress: () => console.log('CANCEL: Email Error Response'),
            },
          ],
          {cancelable: true},
        );
      },
    );
  };

  const yesFunc = () => {
    database().ref(`/users/${auth().currentUser.uid}`).child('favourites').set('');
    setVisible(!visible);
    handleEmail();
    navigation.navigate('SuccessScreen');
  };

  const ListItem = ({item}) => {
    return (
      <View
        style={{
          flexDirection: 'row',
          height: SIZES.height * 0.15,
          borderBottomWidth: 1,
          borderBottomColor: COLORS.borderColor,
        }}>
        <View style={{flex: 0.3, justifyContent: 'center'}}>
          <Image
            source={{uri: item.filename}}
            resizeMode={'contain'}
            style={styles.img}
          />
        </View>
        <View
          style={{flex: 0.5, paddingHorizontal: 15, justifyContent: 'center'}}>
          <Text style={styles.title} numberOfLines={1}>
            {item.title}
          </Text>
          <Text numberOfLines={1} style={styles.quantity}>
            {item.quantity}
          </Text>
        </View>

        <View
          style={{
            flex: 0.35,
            alignItems: 'flex-end',
            justifyContent: 'center',
          }}>
          <View style={{paddingHorizontal: 10}}>
            <Text style={styles.price}>Rs. {item.price}</Text>
          </View>
        </View>
      </View>
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      {loading ? (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <ActivityIndicator size="large" color={COLORS.primaryColor} />
        </View>
      ) : (
        <View>
          {datas.length !== 0 ? (
            <View style={{paddingHorizontal: 10}}>
              <FlatList
                contentContainerStyle={styles.flatListSyle}
                data={datas}
                renderItem={ListItem}
                showsVerticalScrollIndicator={false}
              />

              <View style={styles.buttonView}>
                <View style={{paddingHorizontal: 30}}>
                  <CommonButton
                    buttonColor={COLORS.primaryColor}
                    text={'Buy All'}
                    width={SIZES.width * 0.35}
                    onPress={() => onBuy()}
                  />
                </View>

                <View>
                  <CommonButton
                    buttonColor={COLORS.primaryColor}
                    text={'Remove All'}
                    width={SIZES.width * 0.35}
                    onPress={() => onRemove()}
                  />
                </View>
              </View>
            </View>
          ) : (
            <View>
              <View style={{justifyContent: 'center', alignItems: 'center'}}>
                <Image
                  style={styles.image}
                  resizeMode={'contain'}
                  source={Images.greenHeart}
                />
              </View>
              <View style={styles.txtView}>
                <Text style={styles.emptyTitle}>No favourites yet !</Text>
                <Text style={styles.emptyText}>
                  Like a product you see ? Save {'\n'} them here to your
                  favourites.
                </Text>
              </View>
              <View style={styles.buttonView2}>
                <CommonButton
                  buttonColor={COLORS.primaryColor}
                  text={'Start exploring'}
                  width={SIZES.width * 0.9}
                  onPress={() => navigation.navigate('HomeScreen')}
                />
              </View>
            </View>
          )}
        </View>
      )}

      <CommonPopup
        title={'Do You Want to buy all these items ?'}
        subtitle={'Total amount to be paid :'}
        text={sum}
        type={1}
        isVisible={visible}
        successButtonText={'Yes'}
        negativeButtonText={'No'}
        onPressNo={() => setVisible(!visible)}
        onPressYes={yesFunc}
      />
    </SafeAreaView>
  );
};

export default Favourites;

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.whiteColor,
  },

  ButtonStyle: {
    marginTop: '10@vs',
    height: '32@vs',
    width: '87@s',
    borderRadius: '5@msr',
    backgroundColor: COLORS.whiteColor,
    justifyContent: 'center',
    alignSelf: 'flex-start',
  },

  button: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: '5@s',
  },
  total: {
    fontSize: SIZES.h4,
    color: '#87898E',
    alignSelf: 'center',
  },
  iconMinus: {
    color: '#9B9B9B',
  },
  iconPlus: {
    color: COLORS.primaryColor,
  },
  priceView: {
    flexDirection: 'row',
    //marginTop:SIZES.base,
    borderBottomColor: COLORS.borderColor,
    borderBottomWidth: '0.8@s',
    paddingVertical: '18@vs',
  },
  productTitle: {
    //...FONTS.P0,
    fontSize: adjust(13),
    fontWeight: '500',
    color: COLORS.blackColor,
    lineHeight: '20@vs',
  },

  flatListSyle: {
    paddingTop: '15@vs',
    paddingBottom: '140@vs',
  },
  img: {
    height: '80@vs',
    width: '80@s',
    alignSelf: 'center',
  },
  title: {
    fontSize: adjust(14),
    fontWeight: '500',
    color: COLORS.blackColor,
    lineHeight: '20@vs',
  },
  quantity: {
    ...FONTS.body4,
    fontSize: adjust(12),
    lineHeight: '18@vs',
    color: COLORS.textColor,
  },
  price: {
    //...FONTS.P0,
    fontSize: adjust(15),
    fontWeight: '500',
    color: COLORS.blackColor,
  },
  buttonView: {
    alignItems: 'center',
    paddingTop: '15@vs',
    flexDirection: 'row',
    paddingBottom: '80@vs',
    position: 'absolute',
    top: '480@vs',
    right: '10@s',
    left: '10@s',
  },

  buttonViewFav: {
    alignItems: 'center',
    paddingTop: '15@vs',
    paddingBottom: '80@vs',
    position: 'absolute',
    top: '410@vs',
    right: '10@s',
    left: '10@s',
  },

  //empty

  image: {
    height: '150@vs',
    width: '160@vs',
    marginTop: '55@vs',
    // marginHorizontal:"45@s",
  },
  txtView: {
    paddingVertical: '40@vs',
    alignItems: 'center',
  },
  emptyTitle: {
    textAlign: 'center',
    lineHeight: '23@vs',
    ...FONTS.h3,
    color: COLORS.blackColor,
  },
  emptyText: {
    textAlign: 'center',
    lineHeight: '18@vs',
    ...FONTS.h5,
    color: COLORS.textColor,
    marginVertical: '18@vs',
  },
  buttonView2: {
    alignItems: 'center',
    marginTop: '85@vs',
  },
});
