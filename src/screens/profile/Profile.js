import {Text, View, SafeAreaView, TouchableOpacity} from 'react-native';
import React, {useContext, useState} from 'react';
import {ScaledSheet} from 'react-native-size-matters';
import auth from '@react-native-firebase/auth';
import ProfileIcon from 'react-native-vector-icons/MaterialIcons';
import {COLORS, Vector, Images, FONTS, adjust, SIZES} from '../../constants';
import {CommonButton, CommonPopup} from '../../components';
import {AuthContext} from '../../navigation/AuthProvider';

const FlatListData = [
  {
    id: 1,
    title: 'My cart',
  },
  {
    id: 2,
    title: 'Favourites',
  },
  {
    id: 3,
    title: 'Terms And Conditions',
  },
  {
    id: 4,
    title: 'Privacy Policy',
  },
];

const Profile = ({navigation}) => {
  const { logout} = useContext(AuthContext);

  const [visible, setVisible] = useState(false);

  const clickAction = item => {
    if (item.id == 1) {
      navigation.navigate('CartStackScreen');
    } else if (item.id == 2) {
      navigation.navigate('FavouritesStackScreen');
    } else if (item.id == 3) {
      navigation.navigate('TermsAndConditions');
    } else if (item.id == 4) {
      navigation.navigate('PrivacyPolicy');
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.buttonView}>
        <View style={styles.profileView}>
          <View style={{flexDirection: 'row'}}>
            <ProfileIcon
              name="account-circle"
              size={80}
              color={COLORS.primaryColor}
            />
            <View style={styles.nameView}>
              <Text style={styles.profileTitle}>{auth().currentUser.displayName}</Text>
              {/* <View style={{marginTop:2}}>
  {Vector.Edit}
  </View> */}

              <Text style={styles.profileTxt}>{auth().currentUser.email}</Text>
            </View>
          </View>
        </View>
        <View>
          {FlatListData.map((item, index) => {
            return (
              <TouchableOpacity
                key={item.id}
                // activeOpacity={0.6}
                style={styles.flatListParentView}
                onPress={() => clickAction(item)}>
                <View style={{flex: 0.8, justifyContent: 'center'}}>
                  <Text numberOfLines={1} style={styles.tabTitle}>
                    {item.title}
                  </Text>
                </View>
                <View style={{flex: 0.2, alignItems: 'flex-end'}}>
                  {Vector.DrawerRight}
                </View>
              </TouchableOpacity>
            );
          })}
        </View>
      </View>

      <TouchableOpacity style={styles.buttonBottomView}>
        <View style={styles.logoutIcon}>{Vector.Logout}</View>
        <CommonButton
          secondary={true}
          buttonColor={'#E2E2E2'}
          width={SIZES.width * 0.9}
          text={'Log Out'}
          onPress={() => setVisible(!visible)}
        />
      </TouchableOpacity>

      <CommonPopup
        title={'Logout'}
        subtitle={'Are you sure? You want to logout?'}
        isVisible={visible}
        successButtonText={'Logout'}
        negativeButtonText={'Cancel'}
        onPressNo={() => setVisible(!visible)}
        onPressYes={() => logout()}
      />
    </SafeAreaView>
  );
};

export default Profile;

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.whiteColor,
  },

  profileTitle: {
    lineHeight: '24@vs',
    fontSize: adjust(16),
    fontWeight: '500',
    color: COLORS.blackColor,
    paddingRight: '10@vs',
  },
  profileTxt: {
    ...FONTS.h5,
    color: COLORS.textColor,
  },
  nameView: {
    justifyContent: 'center',
    paddingHorizontal: '10@s',
  },
  buttonView: {
    paddingHorizontal: '15@s',
  },
  profileView: {
    borderBottomWidth: 0.6,
    borderBottomColor: COLORS.primaryColor,
    paddingVertical: '20@vs',
  },
  locationTitle: {
    ...FONTS.body2,
    fontSize: adjust(11),
    color: COLORS.blackColor,
    lineHeight: '25@vs',
  },
  locationAddress: {
    ...FONTS.body5,
    color: COLORS.blackColor,
  },

  flatListParentView: {
    flexDirection: 'row',
    paddingVertical: '18@vs',
    borderBottomWidth: 0.6,
    borderBottomColor: COLORS.primaryColor,
  },
  tabTitle: {
    ...FONTS.body3,
    color: COLORS.blackColor,
  },
  buttonBottomView: {
    marginVertical: '75@vs',
    alignSelf: 'center',
  },
  logoutIcon: {
    position: 'absolute',
    top: '14@vs',
    left: '28@s',
    zIndex: 1,
  },
});
