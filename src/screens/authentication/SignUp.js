import React, {useState, useContext, useRef} from 'react';
import {
  SafeAreaView,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  Alert,
} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import Snackbar from 'react-native-snackbar';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import auth from '@react-native-firebase/auth';
import {
  COLORS,
  FONTS,
  Vector,
  Images,
  PlaceHolders,
  SIZES,
  adjust,
} from '../../constants';
import {CommonButton, TextInputField} from '../../components';
import {
  dimen_size_height,
  dimen_size_width,
  validateEmail,
  snackBar,
} from '../../utils';
import {Loading} from '../../components';
import {AuthContext} from '../../navigation/AuthProvider';

const SignUp = ({navigation}) => {
  const [name, setName] = React.useState('');
  const [email, setEmail] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [secureTextEntry, setSecureTextEntry] = React.useState(true);

  const [loading, setLoading] = React.useState(false);

  const input1 = useRef();
  const input2 = useRef();
  const input3 = useRef();

  const UpdatePasswordVisiblity = () => {
    setSecureTextEntry(!secureTextEntry);
  };

  //const {register} = useContext(AuthContext);

 const register = async (name, email, password) => {
    if (name === '') {
      Snackbar.show({
        text: 'Username is required !',
        duration: Snackbar.LENGTH_LONG,
        textColor: COLORS.whiteColor,
        backgroundColor: COLORS.errorColor,
        action: {
          text: 'Ok',
          textColor: COLORS.whiteColor,
          onPress: () => Snackbar.dismiss(),
        },
      })
      //snackBar('Username is required');
    } else if (email === '') {
      Snackbar.show({
        text: 'E-mail address is required !',
        duration: Snackbar.LENGTH_LONG,
        textColor: COLORS.whiteColor,
        backgroundColor: COLORS.errorColor,
        action: {
          text: 'Ok',
          textColor: COLORS.whiteColor,
          onPress: () => Snackbar.dismiss(),
        },
      })
      //snackBar('E-mail address is required');
    } else if (!validateEmail(email)) {
      Snackbar.show({
        text: 'E-mail address is invalid !',
        duration: Snackbar.LENGTH_LONG,
        textColor: COLORS.whiteColor,
        backgroundColor: COLORS.errorColor,
        action: {
          text: 'Ok',
          textColor: COLORS.whiteColor,
          onPress: () => Snackbar.dismiss(),
        },
      })
      //snackBar('E-mail address is invalid');
    } else if (password === '') {
      Snackbar.show({
        text: 'Password is required !',
        duration: Snackbar.LENGTH_LONG,
        textColor: COLORS.whiteColor,
        backgroundColor: COLORS.errorColor,
        action: {
          text: 'Ok',
          textColor: COLORS.whiteColor,
          onPress: () => Snackbar.dismiss(),
        },
      })
      //snackBar('Password is required');
    } else {
      setLoading(true);
      try {
        await auth()
          .createUserWithEmailAndPassword(email, password)
          .then(async res => {
            const update = {
              displayName: name,
            };
            await res.user.updateProfile(update);

            const reference = database()
              .ref(`/users/${auth().currentUser.uid}`)
              .set({
                name: name,
                email: email,
                uid: auth().currentUser.uid,
              });
            setLoading(false);
            snackBar('Successfully Registered!');
          });
      } catch (error) {
        if (error.code === 'auth/email-already-in-use') {
          Snackbar.show({
            text: 'E-mail address is already in use!',
            duration: Snackbar.LENGTH_LONG,
            textColor: COLORS.whiteColor,
            backgroundColor: COLORS.errorColor,
            action: {
              text: 'Ok',
              textColor: COLORS.whiteColor,
              onPress: () => Snackbar.dismiss(),
            },
          })
          //snackBar('E-mail address is already in use!');
        }
      }
      setLoading(false);
    }
  }

  return (
    <SafeAreaView style={styles.container}>
        {loading ? <Loading /> :
      <KeyboardAwareScrollView
        keyboardShouldPersistTaps={'handled'}
        scrollEnabled={true}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{flexGrow: 1}}>
        <View style={{alignItems: 'center', marginTop: 45, marginBottom: 50}}>
          <Image
            source={Images.logo}
            style={{height: SIZES.height * 0.1, width: SIZES.width * 0.1}}
            resizeMode={'contain'}
          />
        </View>
        <View style={{paddingHorizontal: 20}}>
          <Text style={styles.title}>{PlaceHolders.signUp}</Text>
          <Text style={styles.subTitle}>{PlaceHolders.signUpText}</Text>
          <Text style={styles.details}>{'Username'}</Text>
          <View
            style={{
              height: dimen_size_height(10),
            }}>
            <TextInputField
              onChangeText={text => setName(text)}
              value={name}
              height={SIZES.padding2 * 4.5}
              returnKeyType="next"
              onSubmitEditing={() => {
                input2.current.focus();
              }}
              blurOnSubmit={false}
              sref={input1}
            />
          </View>
          <Text style={styles.details}>{'Email'}</Text>
          <View
            style={{
              height: dimen_size_height(10),
            }}>
            <TextInputField
              onChangeText={text => setEmail(text)}
              value={email}
              keyboardType={'email-address'}
              autoCapitalize={'none'}
              height={SIZES.padding2 * 4.5}
              returnKeyType="next"
              onSubmitEditing={() => {
                input3.current.focus();
              }}
              blurOnSubmit={false}
              sref={input2}
            />
          </View>

          <Text style={styles.details}>{'Password'}</Text>
          <View
            style={{
              height: dimen_size_height(10),
            }}>
            <TouchableOpacity
              onPress={() => UpdatePasswordVisiblity()}
              style={styles.eyeIcon}>
              {secureTextEntry ? Vector.EyeOff : Vector.Eye}
            </TouchableOpacity>
            <TextInputField
              onChangeText={text => setPassword(text)}
              value={password}
              secureTextEntry={secureTextEntry ? true : false}
              paddingRight={SIZES.padding * 4}
              height={SIZES.padding2 * 4.5}
              sref={input3}
            />
          </View>

          <View style={styles.buttonView}>
            <CommonButton
              buttonColor={COLORS.primaryColor}
              text={PlaceHolders.signUp}
              onPress={() => register(name, email, password)}
            />
          </View>

          <View style={styles.bottomView}>
            <Text style={styles.bottomTxt}>
              {PlaceHolders.alreadyUser}
              {'  '}
            </Text>
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('Login');
              }}>
              <Text style={styles.create}>{PlaceHolders.loginTitle}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </KeyboardAwareScrollView> }
    </SafeAreaView>
  );
};

export default SignUp;

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor:COLORS.whiteColor,
  },
  title: {
    ...FONTS.largeTitle,
    color: COLORS.blackColor,
  },
  subTitle: {
    ...FONTS.body3,
    marginVertical: '10@vs',
    color: COLORS.textColor,
  },
  details: {
    ...FONTS.body3,
    marginTop: '5@vs',
    color: COLORS.textColor,
  },
  eyeIcon: {
    position: 'absolute',
    top: '10@vs',
    right: '2@s',
    zIndex: 1,
  },
  textWrap: {
    lineHeight: '21@vs',
    fontSize: adjust(13),
    paddingRight: '10@s',
    letterSpacing: 0.2,
  },
  buttonView: {
    marginVertical: '18@vs',
  },
  bottomView: {
    flexDirection: 'row',
    alignSelf: 'center',
  },
  bottomTxt: {
    ...FONTS.P0,
    color: COLORS.blackColor,
  },
  create: {
    ...FONTS.P0,
    color: COLORS.primaryColor,
  },
});
