import {Text, View, SafeAreaView,Image,ImageBackground} from 'react-native';
import React from 'react';
import {ScaledSheet} from 'react-native-size-matters';
import {COLORS, FONTS, Vector, Images, PlaceHolders, SIZES, adjust} from '../../constants';
import { CommonButton } from '../../components';
const WelcomeScreen = ({navigation}) => {
  return (
    <SafeAreaView style={styles.container}>
      <ImageBackground
         style={{height: SIZES.height, width:SIZES.width}}
        resizeMode={'cover'}
        source={Images.welcome}
      >
     
        <View style={{alignItems:'center',height: SIZES.height /2,paddingVertical:15,marginTop:SIZES.height /2 }}>
        <Image 
         style={{height: SIZES.height * 0.1, width:SIZES.width * 0.1,}}
         resizeMode={'contain'}
         source={Images.logoWhite}
        />
         <Text style={styles.welcome}>Welcome {'\n'} to our store</Text>
         <Text style={styles.txt}>Ger your groceries in as fast as one hour</Text>
<View style={styles.buttonView}>
        <CommonButton
        buttonColor={COLORS.primaryColor}
        text={"Get Started"}
        onPress={() => {navigation.navigate("Login")}}
        width= {SIZES.width * 0.85}
        /> 
        </View>
         </View>
        </ImageBackground>
     
    </SafeAreaView>
  );
};
export default WelcomeScreen;
const styles = ScaledSheet.create ({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  welcome: {
    color: COLORS.whiteColor,
    ...FONTS.h1,
//fontSize:adjust(30),
textAlign:'center'
  },
  txt: {
    color: COLORS.whiteColor,
    ...FONTS.h0,
//fontSize:adjust(30),
textAlign:'center',
lineHeight:"22@vs",
  },
  buttonView:{
paddingVertical:"25@vs",
  },
});





