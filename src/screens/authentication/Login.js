import React, {useState, useContext, useRef} from 'react';
import {
  SafeAreaView,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  Alert,
} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {
  COLORS,
  FONTS,
  Vector,
  Images,
  PlaceHolders,
  SIZES,
  adjust,
} from '../../constants';
import database from '@react-native-firebase/database';
import auth from '@react-native-firebase/auth';
import Snackbar from 'react-native-snackbar';
import {CommonButton, TextInputField} from '../../components';
import {
  dimen_size_height,
  dimen_size_width,
  validateEmail,
  snackBar,
} from '../../utils';
import {Loading} from '../../components';
import {AuthContext} from '../../navigation/AuthProvider';

const Login = ({navigation}) => {
  const [name, setName] = React.useState('');
  const [email, setEmail] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [secureTextEntry, setSecureTextEntry] = React.useState(true);

  const [loading, setLoading] = React.useState(false);

  const input1 = useRef();
  const input2 = useRef();

  const UpdatePasswordVisiblity = () => {
    setSecureTextEntry(!secureTextEntry);
  };

  //const {login} = useContext(AuthContext);

 const login = async (email, password, name) => {
    if (email === '') {
      Snackbar.show({
        text: 'E-mail address is required !',
        duration: Snackbar.LENGTH_LONG,
        textColor: COLORS.whiteColor,
        backgroundColor: COLORS.errorColor,
        action: {
          text: 'Ok',
          textColor: COLORS.whiteColor,
          onPress: () => Snackbar.dismiss(),
        },
      })
      // snackBar('E-mail address is required');
    } else if (!validateEmail(email)) {
      Snackbar.show({
        text: 'E-mail address is invalid !',
        duration: Snackbar.LENGTH_LONG,
        textColor: COLORS.whiteColor,
        backgroundColor: COLORS.errorColor,
        action: {
          text: 'Ok',
          textColor: COLORS.whiteColor,
          onPress: () => Snackbar.dismiss(),
        },
      })
      //snackBar('E-mail address is invalid');
    } else if (password === '') {
      Snackbar.show({
        text: 'Password is required !',
        duration: Snackbar.LENGTH_LONG,
        textColor: COLORS.whiteColor,
        backgroundColor:COLORS.errorColor,
        action: {
          text: 'Ok',
          textColor: COLORS.whiteColor,
          onPress: () => Snackbar.dismiss(),
        },
      })
      //snackBar('Password is required');
    } else {
      setLoading(true);
      try {
        await auth()
          .signInWithEmailAndPassword(email, password)
          .then(async res => {
            const update = {
              displayName: name,
            };
            await res.user.updateProfile(update);
            setLoading(false);
            snackBar('Logged in Successfully!');
          });
      } catch (error) {
        if (error.code === 'auth/wrong-password') {
          Snackbar.show({
            text: 'E-mail address/Password is incorrect!',
            duration: Snackbar.LENGTH_LONG,
            textColor: COLORS.whiteColor,
            backgroundColor: COLORS.errorColor,
            action: {
              text: 'Ok',
              textColor: COLORS.whiteColor,
              onPress: () => Snackbar.dismiss(),
            },
          })
          //snackBar('E-mail address/Password is incorrect!');
        }
        if (error.code === 'auth/user-not-found') {
          Snackbar.show({
            text: 'E-mail is not registered!!',
            duration: Snackbar.LENGTH_LONG,
            textColor: COLORS.whiteColor,
            backgroundColor: COLORS.errorColor,
            action: {
              text: 'Ok',
              textColor: COLORS.whiteColor,
              onPress: () => Snackbar.dismiss(),
            },
          })
          //snackBar('E-mail is not registered!');
        }
      }
      setLoading(false);
    }
  }

  return (
    <SafeAreaView style={styles.container}>
        {loading ? <Loading /> :
      <KeyboardAwareScrollView
        keyboardShouldPersistTaps={'handled'}
        scrollEnabled={true}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{flexGrow: 1}}>
        <View style={{alignItems: 'center', marginTop: 45, marginBottom: 50}}>
          <Image
            source={Images.logo}
            style={{height: SIZES.height * 0.1, width: SIZES.width * 0.1}}
            resizeMode={'contain'}
          />
        </View>
        <View style={{paddingHorizontal: 20}}>
          <Text style={styles.title}>{PlaceHolders.loginTitle}</Text>
          <Text style={styles.subTitle}>{PlaceHolders.signUpText}</Text>

          <Text style={styles.details}>{'Email'}</Text>
          <View
            style={{
              height: dimen_size_height(10),
            }}>
            <TextInputField
              onChangeText={text => setEmail(text)}
              value={email}
              keyboardType={'email-address'}
              autoCapitalize={'none'}
              height={SIZES.padding2 * 4.5}
              returnKeyType="next"
              onSubmitEditing={() => {
                input2.current.focus();
              }}
              blurOnSubmit={false}
              sref={input1}
            />
          </View>

          <Text style={styles.details}>{'Password'}</Text>
          <View
            style={{
              height: dimen_size_height(10),
            }}>
            <TouchableOpacity
              onPress={() => UpdatePasswordVisiblity()}
              style={styles.eyeIcon}>
              {secureTextEntry ? Vector.EyeOff : Vector.Eye}
            </TouchableOpacity>
            <TextInputField
              onChangeText={text => setPassword(text)}
              value={password}
              secureTextEntry={secureTextEntry ? true : false}
              paddingRight={SIZES.padding * 4}
              height={SIZES.padding2 * 4.5}
              sref={input2}
            />
          </View>
          {/* <TouchableOpacity onPress={() => {}} style={{alignSelf: 'flex-end'}}>
            <Text style={styles.forgotPassword}>
              {PlaceHolders.forgotPassword}
            </Text>
          </TouchableOpacity> */}

          <View style={styles.buttonView}>
            <CommonButton
              buttonColor={COLORS.primaryColor}
              text={'Log In'}
              onPress={() => login(email, password)}
            />
          </View>
          {/* <Text style={styles.orTxt}>{'Or connect with social media'}</Text>
          <TouchableOpacity style={styles.buttonView}>
            <View style={styles.googleIcon}>{Vector.Google}</View>
            <CommonButton
              buttonColor={COLORS.buttonColor}
              text={'Continue with Google'}
              onPress={() => {}}
            />
          </TouchableOpacity> */}

          <View style={styles.bottomView}>
            <Text style={styles.bottomTxt}>
              {PlaceHolders.newUser}
              {'  '}
            </Text>
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('SignUp');
              }}>
              <Text style={styles.create}>{PlaceHolders.signUp}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </KeyboardAwareScrollView> }
    </SafeAreaView>
  );
};

export default Login;

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor:COLORS.whiteColor,
  },
  title: {
    ...FONTS.largeTitle,
    color: COLORS.blackColor,
  },
  subTitle: {
    ...FONTS.body3,
    marginVertical: '10@vs',
    color: COLORS.textColor,
  },
  details: {
    ...FONTS.body3,
    marginTop: '5@vs',
    color: COLORS.textColor,
  },
  eyeIcon: {
    position: 'absolute',
    top: '10@vs',
    right: '2@s',
    zIndex: 1,
  },
  buttonView: {
    marginVertical: '65@vs',
  },
  bottomView: {
    flexDirection: 'row',
    alignSelf: 'center',
  },
  bottomTxt: {
    ...FONTS.P0,
    color: COLORS.blackColor,
  },
  create: {
    ...FONTS.P0,

    color: COLORS.primaryColor,
  },
  forgotPassword: {
    ...FONTS.P0,
    color: COLORS.blackColor,
  },
  orTxt: {
    textAlign: 'center',
    color: COLORS.textColor,
    ...FONTS.body4,
  },
  googleIcon: {
    position: 'absolute',
    top: '14@vs',
    left: '28@s',
    zIndex: 1,
  },
});
