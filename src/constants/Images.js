// Image
const logoWhite = require('../../assets/images/logoWhite.png');
const logo = require('../../assets/images/logo.png');
const welcome = require('../../assets/images/welcome.png');
const banana = require('../../assets/images/banana.png');
const success = require('../../assets/images/success.png');
const emptyCart = require('../../assets/images/emptyCart.png');
const greenHeart = require('../../assets/images/greenHeart.png');

//Icon
const plus = require('../../assets/icons/plusIcon.png');

export default {
  logoWhite,
  logo,
  welcome,
  banana,
  plus,
  success,
  emptyCart,
  greenHeart,
};
