/* eslint-disable react-native/no-inline-styles */
// Vector Icons
import React from "react";
import { COLORS, SIZES } from "./Theme";
import MenuIcon from "react-native-vector-icons/Ionicons";
import NotificationIcon from "react-native-vector-icons/MaterialCommunityIcons";
import CartIcon from "react-native-vector-icons/MaterialCommunityIcons";
import BackIcon from "react-native-vector-icons/AntDesign";
import UpArrowIcon from "react-native-vector-icons/AntDesign";
import BackArrowIcon from "react-native-vector-icons/AntDesign";
import DownArrowIcon from "react-native-vector-icons/AntDesign";
import HeaderBackArrowIcon from "react-native-vector-icons/AntDesign";
import DrawerRightIcon from "react-native-vector-icons/AntDesign";
import Feather from "react-native-vector-icons/Feather";
import CancelIcon from "react-native-vector-icons/Feather";
import CloseIcon from "react-native-vector-icons/Ionicons";
import EyeIcon from "react-native-vector-icons/Ionicons";
import EyeOffIcon from "react-native-vector-icons/Ionicons";
import GoogleIcon from "react-native-vector-icons/FontAwesome";
import AccountIcon from "react-native-vector-icons/MaterialCommunityIcons";
import SearchIcon from "react-native-vector-icons/Ionicons";
import EditIcon from "react-native-vector-icons/Octicons";
import LogoutIcon from "react-native-vector-icons/MaterialIcons";
import OrdersIcon from "react-native-vector-icons/Feather";
import MapIcon from "react-native-vector-icons/MaterialCommunityIcons";
import HeartIconO from "react-native-vector-icons/FontAwesome";
import HeartIcon from "react-native-vector-icons/FontAwesome";

const MenuImage = (
  <MenuIcon name={"ios-menu-sharp"} color={COLORS.whiteColor} size={25} />
);

const NotificationImage = (
  <NotificationIcon name={"bell-outline"} color={COLORS.primaryColor} size={22} />
);

const CartImage = (
  <CartIcon name={"cart"} color={COLORS.whiteColor} size={22} />
);

const Back = (
  <BackIcon name={"left"} color={COLORS.darkGrey} size={SIZES.icon} />
);

const BackArrow = (
  <BackArrowIcon name={"arrowleft"} color={COLORS.primaryColor} size={25} />
);

const HeaderBackArrow = (
  <HeaderBackArrowIcon name={"arrowleft"} color={COLORS.whiteColor} size={25} />
);

const DrawerRight = (
  <DrawerRightIcon name={"right"} color={COLORS.blackColor} size={17} />
);

const cancel = <Feather name={"x-circle"} color={COLORS.darkGrey} size={20} />;

const CancelX = <CancelIcon name={"x"} color={COLORS.darkGrey} size={20} />;

const close = <CloseIcon name={"close"} color={COLORS.blackColor} size={17} />;

const Eye = <EyeIcon name={"eye"} color={COLORS.darkGrey} size={25} />;

const EyeOff = (
  <EyeOffIcon name={"eye-off"} color={COLORS.darkGrey} size={25} />
);

const DownArrow = (
  <DownArrowIcon name={"down"} color={COLORS.darkGrey} size={23} />
);

const UpArrow = <UpArrowIcon name={"up"} color={COLORS.darkGrey} size={23} />;

const Google = <GoogleIcon name={"google"} color={COLORS.whiteColor} size={23} />;

const Account = <AccountIcon name={"account-circle"} color={COLORS.primaryColor} size={32} />;

const Search = <SearchIcon name={"search"} color={COLORS.primaryColor} size={23} />;

const Edit = <EditIcon name={"pencil"} color={COLORS.primaryColor} size={18} />;

const Logout = <LogoutIcon name={"logout"} color={COLORS.primaryColor} size={22} />;

const Orders = <OrdersIcon name={"shopping-bag"} color={COLORS.blackColor} size={22} />;

const Map = <MapIcon name={"map-marker"} color={COLORS.blackColor} size={22} />;

const HeartO = <HeartIconO name={"heart-o"} color={COLORS.textColor} size={20} />;

const Heart = <HeartIcon name={"heart"} color={"#FE251B"} size={20} />;


export default {
  Back,
  MenuImage,
  NotificationImage,
  CartImage,
  BackArrow,
  DrawerRight,
  HeaderBackArrow,
  cancel,
  CancelX,
  close,
  Eye,
  EyeOff,
  DownArrow,
  UpArrow,
  Google,
  Account,
  Search,
  Edit,
  Logout,
  Orders,
  Map,
  HeartO,
  Heart,
};
