//Login
const loginTitle = 'Login';
const welcome = 'Welcome to our store';
const forgotPassword = "Forgot Password ?"
const newUser = "Don’t have an account?";

//SignUp
const signUp = 'Sign Up';
const signUpText = 'Enter your credentials to continue';
const alreadyUser = "Already have an account?";



export default {
  loginTitle,
  welcome,
  signUp,
  signUpText,
  alreadyUser,
  forgotPassword,
  newUser,
};
