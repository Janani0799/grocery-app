import React, {createContext, useState} from 'react';
import auth from '@react-native-firebase/auth';
// import database from '@react-native-firebase/database';
// import {snackBar, validateEmail} from '../utils';
import {Loading} from '../components';

export const AuthContext = createContext();

export const AuthProvider = ({children}) => {
  const [user, setUser] = useState(null);
  // const [loading, setLoading] = useState(false);
  return (
    <AuthContext.Provider
      value={{
        user,
        setUser,
       
      
        logout: async () => {
          try {
            await auth().signOut();
          } catch (e) {
            console.error(e);
          }
        },
      }}>
     {children}
    </AuthContext.Provider>
  );
};
