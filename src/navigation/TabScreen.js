import React, {useRef} from 'react';
import {View, Image} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {COLORS, FONTS, Fonts, SIZES, Images} from '../constants';
import {CommonHeader} from '../components';
import Favourites from '../screens/favourites/Favourites';
import Cart from '../screens/cart/Cart';
import Home from '../screens/dashboard/Home';
import Profile from '../screens/profile/Profile';
import Search from '../screens/search/Search';
import ProductList from '../screens/dashboard/ProductList';
import ProductCategory from '../screens/dashboard/ProductCategory';
import CategoryList from '../screens/dashboard/CategoryList';
import ProductDetails from '../screens/dashboard/ProductDetails';
import TermsAndConditions from '../screens/termsAndConditions/TermsAndConditions';
import PrivacyPolicy from '../screens/privacyPolicy/PrivacyPolicy';
import SuccessScreen from '../screens/success/SuccessScreen';

const Tab = createBottomTabNavigator();
const HomeStack = createNativeStackNavigator();
const CartStack = createNativeStackNavigator();
const FavouritesStack = createNativeStackNavigator();
const MainStack = createNativeStackNavigator();
const Main = createNativeStackNavigator();

const TabScreen = ({}) => {
  return (
    <Tab.Navigator
      initialRouteName="HomeScreen"
      screenOptions={{
        tabBarStyle: styles.TabStyleBar,
        tabBarLabelStyle: styles.TabLabelStyle,
      }}>
      <Tab.Screen
        name={'HomeScreen'}
        component={HomeScreen}
        options={{
          headerShown: false,
          tabBarLabel: 'Shop',
          tabBarActiveTintColor: COLORS.primaryColor,
          tabBarIcon: ({color}) => (
            <Ionicons name="home-outline" color={color} size={26} />
          ),
        }}
      />

      <Tab.Screen
        name={'CartStackScreen'}
        component={CartStackScreen}
        options={{
          headerShown: false,
          tabBarLabel: 'Cart',
          tabBarActiveTintColor: COLORS.primaryColor,
          tabBarIcon: ({color}) => (
            <Ionicons name="cart-outline" color={color} size={26} />
          ),
        }}
      />

      <Tab.Screen
        name={'FavouritesStackScreen'}
        component={FavouritesStackScreen}
        options={{
          headerShown: false,
          tabBarLabel: 'Favourite',
          tabBarActiveTintColor: COLORS.primaryColor,
          tabBarIcon: ({color}) => (
            <MaterialIcons name="favorite-outline" color={color} size={26} />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

const MainStackScreen = ({}) => {
  return (
    <Main.Navigator initialRouteName="StackScreen">
      <Main.Screen
        name="StackScreen"
        component={MainScreens}
        options={{headerShown: false}}
      />
    </Main.Navigator>
  );
};

const HomeScreen = ({navigation}) => {
  return (
    <HomeStack.Navigator>
      <HomeStack.Screen
        name={'Home'}
        component={Home}
        options={{
          headerShown: true,
          header: () => (
            <CommonHeader
              type={1}
              title={'Shop'}
              onClick={() => navigation.navigate('Profile')}
              searchNavigation={() => navigation.navigate('Search')}
            />
          ),
        }}
      />
    </HomeStack.Navigator>
  );
};

const CartStackScreen = ({navigation}) => {
  return (
    <CartStack.Navigator>
      <CartStack.Screen
        name={'Cart'}
        component={Cart}
        options={{
          headerShown: true,
          header: () => (
            <CommonHeader
              type={1}
              title={'My Cart'}
              onClick={() => navigation.navigate('Profile')}
              searchNavigation={() => navigation.navigate('Search')}
            />
          ),
        }}
      />
    </CartStack.Navigator>
  );
};

const FavouritesStackScreen = ({navigation}) => {
  return (
    <FavouritesStack.Navigator>
      <FavouritesStack.Screen
        name={'Favourites'}
        component={Favourites}
        options={{
          headerShown: true,
          header: () => (
            <CommonHeader
              type={1}
              title={'Favorurites'}
              onClick={() => navigation.navigate('Profile')}
              searchNavigation={() => navigation.navigate('Search')}
            />
          ),
        }}
      />
    </FavouritesStack.Navigator>
  );
};

const MainScreens = ({navigation}) => {
  return (
    <MainStack.Navigator>
      <MainStack.Screen
        name={'MainScreens'}
        component={TabScreen}
        options={{
          headerShown: false,
        }}
      />
      <MainStack.Screen
        name={'Profile'}
        component={Profile}
        options={{
          headerShown: true,
          header: () => (
            <CommonHeader
              type={2}
              onClick={() => navigation.goBack()}
              title={'Profile'}
            />
          ),
        }}
      />

      <MainStack.Screen
        name={'Search'}
        component={Search}
        options={{
          headerShown: true,
          header: () => (
            <CommonHeader
              type={2}
              onClick={() => navigation.goBack()}
              title={'Find Products'}
            />
          ),
        }}
      />

      <MainStack.Screen
        name={'ProductList'}
        component={ProductList}
        options={{
          headerShown: true,
          header: () => (
            <CommonHeader
              type={2}
              onClick={() => navigation.goBack()}
              title={'Products'}
            />
          ),
        }}
      />

      <MainStack.Screen
        name={'ProductCategory'}
        component={ProductCategory}
        options={{
          headerShown: true,
          header: () => (
            <CommonHeader
              type={2}
              onClick={() => navigation.goBack()}
              title={'Product Category'}
            />
          ),
        }}
      />

      <MainStack.Screen
        name={'CategoryList'}
        component={CategoryList}
        options={{
          headerShown: true,
          header: () => (
            <CommonHeader
              type={2}
              onClick={() => navigation.goBack()}
              title={'Category List'}
            />
          ),
        }}
      />

      <MainStack.Screen
        name={'ProductDetails'}
        component={ProductDetails}
        options={{
          headerShown: true,
          header: () => (
            <CommonHeader
              type={2}
              onClick={() => navigation.goBack()}
              title={'Product Details'}
            />
          ),
        }}
      />

      <MainStack.Screen
        name={'TermsAndConditions'}
        component={TermsAndConditions}
        options={{
          headerShown: true,
          header: () => (
            <CommonHeader
              type={2}
              onClick={() => navigation.goBack()}
              title={'Terms And Conditions'}
            />
          ),
        }}
      />

      <MainStack.Screen
        name={'PrivacyPolicy'}
        component={PrivacyPolicy}
        options={{
          headerShown: true,
          header: () => (
            <CommonHeader
              type={2}
              onClick={() => navigation.goBack()}
              title={'Privacy Policy'}
            />
          ),
        }}
      />

      <MainStack.Screen
        name={'SuccessScreen'}
        component={SuccessScreen}
        options={{
          headerShown: false,
        }}
      />
    </MainStack.Navigator>
  );
};

const styles = ScaledSheet.create({
  TabLabelStyle: {fontSize: '11@s', fontFamily: Fonts.Regular},
  TabStyleBar: {
    position: 'absolute',
    backgroundColor: COLORS.whiteColor,
    height: SIZES.height / 14,
    borderTopLeftRadius: '20@msr',
    borderTopRightRadius: '20@msr',
    paddingVertical: '7@vs',
    shadowColor: COLORS.blackColor,
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 0.46,
    shadowRadius: 11.14,

    elevation: 17,
  },
  TabFocusBar: {
    height: '4.5@vs',
    width: '50@msr',
    backgroundColor: COLORS.primaryColor,
    borderBottomLeftRadius: '20@msr',
    borderBottomRightRadius: '20@msr',
    top: '-4@vs',
  },
  TabParentView: {
    alignItems: 'center',
  },
  TabMarginTop: {marginTop: '5@vs'},
  image: {
    width: '20@s',
    height: '20@vs',
  },
});
export {TabScreen, MainStackScreen, MainScreens};
